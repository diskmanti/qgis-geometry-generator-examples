# Adding dimensions
Adding dimensions is very common in CAD programs, but in a GIS this usually isn't possible.  In QGIS you can add dimensions to polygons or lines using styles.  Of course this will give the best results if your layer doesn't contain too complex items.  Below you can find a styling for polygon layers and one for line layers.

## Dimensions for Polygons
<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dimensions/dimensions_polygon.qml?inline=false"><img src="../../Example_images/dimensions_polygon.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dimensions/dimensions_polygon.qml?inline=false)

## Dimensions for Lines
<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dimensions/dimensions_line.qml?inline=false"><img src="../../Example_images/dimensions_line.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/dimensions/dimensions_line.qml?inline=false)