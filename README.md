# QGIS Geometry Generator examples

I use **Geometry Generator** a lot in [QGIS](https://qgis.org). I promised to make some of my use cases available online: https://twitter.com/michelstuyts/status/1057049855489646592, so here they are. A great introduction to QGIS Geometry Generator by [Klas Karlsson](https://twitter.com/klaskarlsson) can be found on Youtube: https://www.youtube.com/watch?v=0YxjJ-9zIJ0. This repository contains some of my use cases of Geometry Generators.

All examples in this repository were created in **QGIS 3.4**  with both the project and the layer set to the Lambert 72 Belgium CRS ([EPSG:31370](https://epsg.io/31370)), that has meters as units. In the style settings the units to set sizes "Map Units" and "Meters at scale" are frequently used, so in EPSG:31370 both will result in Meters.

Below you will find an example of all styles currently in this repository.  This repository contains all **QML-files** to try the styles yourself. You can also download this entire repository at once: https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/-/archive/master/qgis-geometry-generator-examples-master.zip

## Dimensions for Polygons
<table><tr><td><a href="QML-files/dimensions/"><img src="Example_images/dimensions_polygon.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/dimensions/)

## Dimensions for Lines
<table><tr><td><a href="QML-files/dimensions/"><img src="Example_images/dimensions_line.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/dimensions/)

## Direction 
### One arrow per line of a polyline
<table><tr><td><a href="QML-files/direction/"><img src="Example_images/direction.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/direction/)

## Labels with a leader 
<table><tr><td><a href="QML-files/label_with_leader/"><img src="Example_images/label_with_leader.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/label_with_leader/)

## Pseudo-random points
<table><tr><td><a href="QML-files/pseudo-random_points_edge/"><img src="Example_images/pseudo-random_points_edge.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/pseudo-random_points_edge/)

## Points as buildings
<table><tr><td><a href="QML-files/points_as_building/"><img src="Example_images/points_as_building.png"></a></td></tr></table> 

[Details of this Geometry Generator Style](QML-files/points_as_building/)